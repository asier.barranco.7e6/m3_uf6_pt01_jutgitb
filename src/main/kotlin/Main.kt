//LLibreries
import java.io.File
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

//Ruta de l'arxiu de la base de dades
val dataBaseFile = "./db/asierdatabase.db"

//Variables de colors
const val rojo = "\u001b[31m"
const val verde = "\u001b[32m"
const val azul = "\u001b[34m"
const val reset = "\u001b[0m"

fun main() {
    do {
        mostrarMenuUsuari()
        //L'usuari tria una opció
        print("Tria usuari: "); val user: Int = readLine()!!.toInt()
        when (user) {

            1 -> {
                //Opció alumne
                menuAlumne()
            }
            2 -> {
                //Opció professor, si la contrasenya és correcta, accedeix
                if (contrasenyaProfessor()){
                    menuProfessor()
                }
            }
            //Opció sortir
            0 -> println("Adéu")
            //Qualsevol altra opció
            else -> println("ERROR. Usuari no identificat.")

        }

    } while (user != 0)

}
/**
 * Funció que mostra el menú de tria d'usuari
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun mostrarMenuUsuari() {
    println(
        """
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Alumne                                          |
                || 2. Professor                                       |
                || 0. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent()
        )
}
/**
 * Funció que, una vegada triat l'usuari alumne, mostra i cride les funcions pertinents
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun menuAlumne() {

    print("Introdueix el teu nom: ");
    val username: String = readln()
    var usuari: String = ""
    //Fem la conexió a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        //Seleccionem el nom de la taula usuari on el rol és el d'alumne
        val query = "SELECT nom FROM usuari WHERE rol = 'alumne'"
        val resultat: ResultSet = listStatement.executeQuery(query)
        //Guardem el resultat de la columna "nom" que conté el nom d'usuari
        usuari = resultat.getString("nom")
    }
    //Comprovar si el nom d'usuari que ha introduït el client és igual al corresponent de la base de dades
    if (username == usuari) {
        do {
            println(
                """
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Seguir amb l’itinerari d’aprenentatge           |
                || 2. Llista problemes                                |
                || 3. Consultar històric de problemes resolts         |
                || 4. Ajuda                                           |
                || 5. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent()
            )

            //Seleccionar opció
            print("Tria opció: ");
            val opc: Int = readLine()!!.toInt()
            when (opc) {

                1 -> {
                    //Passem per parámetre el nom de l'usuari
                    seguirItinerari(usuari)
                }

                2 -> {
                    mostrarProblemes()
                    //Passem per parámetre el nom de l'usuari
                    escollirProblema(usuari)
                }

                3 -> {
                    //Passem per parámetre el nom de l'usuari
                    mostrarProgres(usuari)
                }

                4 -> {
                    //Fitxer amb el manual d'instruccions
                    val ajuda = File("./data/ajuda.txt")
                    println(ajuda.readText())
                }

                5 -> println("")
                else -> println("ERROR. Opció desconeguda")
            }

        } while (opc != 5)

        //Si no existeix l'usuari surt un error i torna a demanar.
    } else println("ERROR. Usuari desconegut")
}
/**
 * Funció que, una vegada triat l'usuari professor, mostra i cride les funcions pertinents
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun menuProfessor(){

    do {
        println(
                """
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Afegir nous problemes                           |
                || 2. Reportar la feina del alumne                    |
                || 0. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent()
            )

        //Triar opció
        print("Tria una opció: "); val opc = readLine()!!.toInt()
        when (opc){
            1 -> {
                //Inserir un problema nou a la base de dades
                afegirProblema()
            }
            2 -> {
                //Posar nota al treball de l'alumne
                reportarFeina()
            }
            0 -> println("")
            else -> println("ERROR. Opció desconeguda")
        }

    } while (opc != 0)

}
/**
 * Funció que demana una contrasenya
 * @author Asier Barranco
 * @version 11/03/2023
 * @return Booleà depenent si ha aconseguit encertar la contrasenya
 */
fun contrasenyaProfessor(): Boolean{


    var contrasenya: String = ""
    //Demanem el nom d'usuari per extreure la contrasenya que correspon a aquest usuari
    print("Nom d'usuari: "); val username:String = readln()
    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "SELECT usuari_contrasenya FROM usuari WHERE nom = '$username' AND rol = 'professor';"
        val resultat: ResultSet = listStatement.executeQuery(query)
        //Guardem la contrasenya d'aquest usuari
        contrasenya = resultat.getString("usuari_contrasenya")
    }
    var contrasenyaCorrecta = false
    var intentsContrasenya: Int = 3
    do {

        print("Introdueix la contrasenya: ")
        val intentContrasenya: String = readLine()!!.toString()
        intentsContrasenya--
        if (intentContrasenya == contrasenya) {
            //Contrasenya correcta
            println("Benvingut. Carregant...")
            contrasenyaCorrecta = true
        } else {
            //Contrasenya incorrecta
            if (intentsContrasenya > 0) {
                println("Error. Contrasenya incorrecta. Queden $intentsContrasenya intents")
            } else println("Error. Contrasenya incorrecta. No queden intents.")
        }
    } while (!contrasenyaCorrecta && intentsContrasenya>0)

    return contrasenyaCorrecta
}
/**
 * Funció que va mostrant i resolent els problemes en ordre
 * @author Asier Barranco
 * @version 11/03/2023
 * @param String amb el nom d'usuari
 */
fun seguirItinerari(usuari: String){

    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()

        //Consultem l'ID de l'usuari
        val consultaIDuser = "SELECT usuari_id FROM usuari WHERE nom = '$usuari'"
        val dadesUsuari: ResultSet = listStatement.executeQuery(consultaIDuser)
        val userID = dadesUsuari.getInt("usuari_id")

        //Agafem tota l'informació dels problemes
        val query = "SELECT * FROM PROBLEMA;"
        val resultat: ResultSet = listStatement.executeQuery(query)
        //Mentre hi hagi problemes
        while (resultat.next()) {
            //Guardem els valors de cada problema
            val enunciat = resultat.getString("enunciat")
            val problemaId = resultat.getInt("problema_id")
            val resolt = resultat.getString("solucio")
            val titol: String = resultat.getString("titol")
            //Si no està resolt
            if (resolt == "false") {
                val listStatement: Statement = connection.createStatement()
                //Seleccionem els valors de la taule del joc de proves que corresponen a la id del problema que toca
                val query = "SELECT * FROM joc_de_proves WHERE problema_id=$problemaId;"
                val resultat: ResultSet = listStatement.executeQuery(query)
                //Mentre hagi camps en aquesta taula
                while (resultat.next()) {
                    //Almacenem els valors d'aquest joc de proves
                    val inputPublic = resultat.getString("inputPublic")
                    val outputPublic = resultat.getString("outputPublic")
                    val inputPrivat = resultat.getString("inputPrivat")
                    val outputPrivat = resultat.getString("outputPrivat")

                    //Mostrem l'informació
                    print("Problema $problemaId: $titol")
                    println("Enunciat: $enunciat")
                    println("Joc de proves:")
                    println("Entrada: $inputPublic. Sortida: $outputPublic")
                    println("Exercici:")
                    println("Entrada: $inputPrivat. Sortida: ?")
                    //Demanem una entrada
                    print("Introdueix la teva resposta: ")
                    do {
                        var respostaUsuari: String = readln()
                        val listStatement: Statement = connection.createStatement()
                        //Inserim l'intent corresponent a la taula intents
                        val query = "INSERT INTO intents (problema_id, usuari_id, entradaUsuari) VALUES ($problemaId, $userID, '$respostaUsuari');"
                        var statement = connection.prepareStatement(query)
                        statement.execute()
                        //Si la resposta és correcta
                        if (respostaUsuari == outputPrivat) {
                            println("IMPRESSIONANT!")
                            val listStatement: Statement = connection.createStatement()
                            //Actualitzem el camp de resolt a la taula
                            val query =
                                "UPDATE problema SET solucio='true' WHERE problema_id=$problemaId;"
                            var statement = connection.prepareStatement(query)
                            statement.execute()
                        } else{
                            //Si no l'endevina surt aquest missatge
                            println("Vaja! Aquesta resposta no és correcta.")
                        }
                        //Mentre no l'adivini
                    } while (respostaUsuari != outputPrivat)
                }

            } else {
                //Si ja està resolt es passa al següent
                println("El problema $problemaId ja està resolt")
            }
        }

    }

}
/**
 * Funció mostra tots els problemes
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun mostrarProblemes() {

    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        //Tota l'informació dels problemes
        val query = "SELECT * FROM PROBLEMA;"
        val resultat: ResultSet = listStatement.executeQuery(query)
        //Mentre hi hagi problemes
        while (resultat.next()) {
            //Almacenem els valors
            val problemaId = resultat.getInt("problema_id")
            val resolt = resultat.getString("solucio")
            val titol: String = resultat.getString("titol")

            if (resolt == "false") {
                //Si no està resolt es veu en vermell
                println(rojo + "Problema $problemaId: $titol" + reset)
            } else {
                //Si està resolt es mostra en verd
                println(verde + "Problema $problemaId: $titol" + reset)

            }
        }
    }
}
/**
 * Funció que demana una ID i resol un problema
 * @author Asier Barranco
 * @version 11/03/2023
 * @param String amb el nom d'usuari
 */
fun escollirProblema(usuari: String){

    var end = false
    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->

        val listStatement: Statement = connection.createStatement()

        //Consultem l'id de l'usuari corresponent a aquest nom d'usuari
        val consultaIDuser = "SELECT usuari_id FROM usuari WHERE nom = '$usuari'"
        val dadesUsuari: ResultSet = listStatement.executeQuery(consultaIDuser)
        val userID = dadesUsuari.getInt("usuari_id")

        //Consultem l'última i la primera ID dels problemes que hi ha
        val consultarUltimaID = "SELECT * FROM problema ORDER BY problema_id DESC LIMIT 1;"
        val consultarPrimeraID = "SELECT * FROM problema ORDER BY problema_id ASC LIMIT 1;"

        val ultimaIDProblemes: ResultSet = listStatement.executeQuery(consultarUltimaID)
        val ultimaID = ultimaIDProblemes.getInt("problema_id")

        val primeraIDProblemes: ResultSet = listStatement.executeQuery(consultarPrimeraID)
        val primeraID = primeraIDProblemes.getInt("problema_id")

        do {
            //Demanem l'ID del problema
            print("ID del problema: ");
            val idProblemaEscollit: Int = readLine()!!.toInt()
            //Comprovem si el problema existeix entre la primera i l'última ID
            if (idProblemaEscollit >  ultimaID || idProblemaEscollit < primeraID) {
                //Si no existeix
                println("ERROR. Aquesta ID no existeix")
            } else {

                //Consultem tota l'informació del problema amb l'id que s'ha triat
                val consultaDadesProblema = "SELECT * FROM problema WHERE problema_id=$idProblemaEscollit;"
                val dadesProblema: ResultSet = listStatement.executeQuery(consultaDadesProblema)

                //Guardem els valors
                val titol = dadesProblema.getString("titol")
                val enunciat = dadesProblema.getString("enunciat")

                //Consultem l'informació del joc de proves corresponent
                val consultaJocProvesProblema = "SELECT * FROM joc_de_proves WHERE problema_id=$idProblemaEscollit;"
                val jocProvesDelProblema: ResultSet = listStatement.executeQuery(consultaJocProvesProblema)
                val inputPublic = jocProvesDelProblema.getString("inputPublic")
                val outputPublic = jocProvesDelProblema.getString("outputPublic")
                val inputPrivat = jocProvesDelProblema.getString("inputPrivat")
                val outputPrivat = jocProvesDelProblema.getString("outputPrivat")

                //Mostrem tota l'informació del problema i el joc de proves
                print("Problema $idProblemaEscollit: $titol\n")
                println("Enunciat: $enunciat")
                println("Joc de proves:")
                println("Entrada: $inputPublic. Sortida: $outputPublic")
                println("Exercici:")
                println("Entrada: $inputPrivat. Sortida: ?")


                do {
                    //Demanem una entrada
                    print("Introdueix la teva resposta: ")
                    val respostaUsuari: String = readln()
                    //L'inserim en la taula intents
                    val afegirIntent = "INSERT INTO intents (problema_id, usuari_id, entradaUsuari) VALUES ($idProblemaEscollit, $userID, '$respostaUsuari');"
                    val statement = connection.prepareStatement(afegirIntent)
                    statement.execute()
                    //Si la resposta és correcta
                    if (respostaUsuari == outputPrivat) {
                        println("IMPRESSIONANT!")
                        //Actualitzem el camp a la taula
                        val actualitzarResolt = "UPDATE problema SET solucio='true' WHERE problema_id=$idProblemaEscollit;"
                        val statement2 = connection.prepareStatement(actualitzarResolt)
                        statement2.execute()
                    } else{
                        //Si no l'endevina surt aquest missatge
                        println("Vaja! Aquesta resposta no és correcta.")
                    }
                    //Mentre no l'endevini
                } while (respostaUsuari != outputPrivat)
                end = true
            }
        } while (!end)
    }
}

/**
 * Funció que mostra l'estat dels problemes
 * @author Asier Barranco
 * @version 11/04/2023
 * @param String amb el nom d'usuari
 */
fun mostrarProgres(usuari: String){

    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()

        //Aconseguim l'ID de l'usuari
        val consultaIDuser = "SELECT usuari_id FROM usuari WHERE nom = '$usuari'"
        val dadesUsuari: ResultSet = listStatement.executeQuery(consultaIDuser)
        val userID = dadesUsuari.getInt("usuari_id")

        //Consultem el l'informació dels problemes
        val consultaProblemes = "SELECT * FROM problema;"
        val problemes: ResultSet = listStatement.executeQuery(consultaProblemes)
        //Mentre n'hi hagi problemes
        while (problemes.next()) {
            //Guardem i mostrem l'informació de cada camp
            val resolt = problemes.getString("solucio")
            val titol = problemes.getString("titol")
            val idProb = problemes.getInt("problema_id")
            //Mostrem l'estat del problema
            if (resolt == "true") {
                println(verde + "Problema $idProb: $titol" + reset)
            } else {
                println(rojo + "Problema $idProb: $titol" + reset)
            }
            /*
             //M'hauria agradat fer-ho així, que sortís el número d'intents per cada un però no he sapigut
               val consultaProblemes = "SELECT COUNT(i.entradaUsuari), p.titol, p.problema_id, p.solucio FROM intents i JOIN problema p ON i.problema_id = p.problema_id WHERE i.usuari_id = $userID;"
               val problemes: ResultSet = listStatement.executeQuery(consultaProblemes)
                while (problemes.next()) {
                    val resolt = problemes.getString("solucio")
                    val titol = problemes.getString("titol")
                    val idProb = problemes.getInt("problema_id")
                    if (resolt == "true") {
                         println(verde + "Problema $idProb: $titol" + reset)
                    } else {
                            println(rojo + "Problema $idProb: $titol" + reset)
                    }
                    val numIntents = problemes.getInt("COUNT(i.entradaUsuari)")
                    print(azul+"Número d'intents $numIntents\n"+reset)

                }
            */
        }
    }



}
/**
 * Funció que insereix un nou problema
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun afegirProblema(){

    //Demanem totes les dades del nou problema
    print("Títol: "); val titol = readln()!!
    print("Enunciat: "); val enunciat = readln()!!
    print("Input públic: "); val inputPublic = readln()!!
    print("Output públic: "); val outputPublic = readln()!!
    print("Input privat: "); val inputPrivat = readln()!!
    print("Output privat: "); val outputPrivat = readln()!!

    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->

        val listStatement: Statement = connection.createStatement()
        //Inserim les dades
        val query = "INSERT INTO problema (titol, enunciat) VALUES ('$titol', '$enunciat');"
        val statement = connection.prepareStatement(query)
        //Executem
        statement.execute()
        //Consultem l'ID d'aquest problema
        val consultaIDProblemaNou = "SELECT * FROM problema ORDER BY problema_id DESC LIMIT 1;"
        val obtenirIDProblemaNou: ResultSet = listStatement.executeQuery(consultaIDProblemaNou)
        val problemaNouID = obtenirIDProblemaNou.getInt("problema_id")

        //Amb aquesta ID podem inserir els valors en la taula joc_de_proves
        val query2 =
            "INSERT INTO joc_de_proves (inputPublic, outputPublic, inputPrivat, outputPrivat, problema_id) VALUES ('$inputPublic', '$outputPublic', '$inputPrivat', '$outputPrivat', $problemaNouID);"
        val statement2 = connection.prepareStatement(query2)
        statement2.execute()

        //Consultem les dades per mostrar-li a l'usuari
        val query3 = "SELECT * FROM problema WHERE problema_id = $problemaNouID;"
        val dadesProblemaNou: ResultSet = listStatement.executeQuery(query3)
        val titolProblemaNou = dadesProblemaNou.getString("titol")

        //Si existeix el títol del nou problema mostrem que s'ha inserir amb èxit
        if (titolProblemaNou.length > 0) {
            println("El problema $titolProblemaNou s'ha inserit amb èxit!")
        }
    }
}
/**
 * Funció que comprova l'estat dels problemes i treu una nota corresponent
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun reportarFeina() {

    //Demanem l'ID de l'usuari al que es vol posar nota
    print("Introduiex l'ID que vols evaluar: "); val usu_id = readln()
    var notaTotal = 0.0
    //Ens connectem a la base de dades
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        //Consultem les dades de l'usuari
        val consulta = "SELECT * FROM usuari WHERE usuari_id = $usu_id;"
        val dadesConsulta: ResultSet = listStatement.executeQuery(consulta)
        val rolUsuari = dadesConsulta.getString("rol")
        //Si el seu rol és el d'alumne
        if (rolUsuari == "alumne") {
            //Seleccionem quants problemes té resolts
            val consultarTotalResolts = "SELECT COUNT(problema_id) FROM problema WHERE solucio = 'true';"
            val numeroProblemesResolts: ResultSet = listStatement.executeQuery(consultarTotalResolts)
            val problemesResolts = numeroProblemesResolts.getInt("COUNT(problema_id)")
            //si ha resolt menys de 5 no és avaluable
            if (problemesResolts > 5) {
                //consultem quants intents ha necessitat per resoldre cada un dels problemes
                val query =
                    "SELECT MAX(i.intents_id), i.entradaUsuari, p.problema_id FROM intents i JOIN problema p ON i.problema_id = p.problema_id WHERE p.solucio='true';"
                val resultat: ResultSet = listStatement.executeQuery(query)
                while (resultat.next()) {
                    //guardem les dades
                    var notaProblema: Double = 0.0
                    val problemaID = resultat.getInt("problema_id")
                    val entradesUsuari = resultat.getString("entradaUsuari")
                    val numIntents = resultat.getInt("MAX(i.intents_id)")
                    //Mostrem les dades al client
                    println("Problema $problemaID: ")
                    println("Última entrada: $entradesUsuari")
                    println("Número total d'intents: $numIntents")

                    //Assignem una nota depenent dels intents
                    notaProblema += when(numIntents) {
                        1 -> 10.0
                        2 -> 9.0
                        3 -> 7.5
                        4 -> 6.0
                        5 -> 5.0
                        else -> 2.5
                    }
                    //La nota del problema
                    println("Nota del problema: $notaProblema")
                    //Sumem la nota a la nota total
                    notaTotal+=notaProblema
                }
                //La nota final és la suma de totes les notes entre la quantitat de resolts
                val notaFinal = notaTotal/problemesResolts
                println("NOTA FINAL DE L'ALUMNE: ${notaFinal*10}")

            } else println("L'usuari no ha resolt prou problemes com per tenir una nota")

            //Si l'usuari no té el rol d'alumne o no existeix no es fa res
        } else println("ERROR. Aquest usuari no és un alumne o no existeix")
    }
}