# M3_UF6_JUTGITB

- [Introducció](#introduccio)
- [Objetius UF6](#objetius)
- [Funcions](#funcions)
- [Base de dades](#bbdd)
- [Conclusions i millores](#conclusions)

## Introducció <a id="introduccio"></a>
Des del departament de programació de l’Institut Tecnològic de Barcelona té la imperiosa necessitat d’implementar un jutge de codi, per tal de facilitar als estudiants de les diferents assignatures de programació del centre amb el seu aprenentatge.
El problema és que els professors d’aquest departament estan desbordats per la quantitat ingent de pràctiques, projectes i exàmens a corregir i no tenen temps de posar-se a programar. Així doncs, han respirat fondo i han decidit cedir aquesta responsabilitat a l’alumnat.
Ha quedat demostrat que la solució que havia presentat anteriorment és altament satisfactòria. Així doncs, per tal de fer una aplicació escalable volem transformar la persistència de dades de fitxers json a una base de dades, per exemple, del tipus SQLite.



## Objetius UF6 <a id="objetius"></a>
### - Crear base de dades: 
El primer que farem és replicar el model de dades que tenim implementat en una base de dades SQLite. Haurem de crear les diferents taules (problema, usuari, joc de proves, intent…) així com les seves relacions entre elles

### - Conectar la Base de dades a l’aplicació: 
Per connectar la base de dades usarem la interfície JDBC que heu treballat a classe.

### - Implementar les crides de consulta i edició d’informació: 
Per acabar amb la persistència de dades, volem implementar les funcions que ens serviran per explotar la informació que tenim a la base de dades. Volem implementar totes les operacions típiques de CRUD (Create, Read, Update, Delete) en relació de les diferents taules que tingueu a la vostra aplicació. Seria interessant fer una classe d’interfície on estiguessin totes les funcions que tinguessin relació amb la gestió de les dades

## Funcions<a id="funcions"></a>

##### Funcions implementades:

    -   mostrarMenuUsuari -> Mostra el menú principal
    -   menuAlumne -> Mostra i fa les funcions del menú de l'alumne
    -   menuProfessor -> Mostra i fa les funcions del menú del professor
    -   contrasenyaProfessor -> Demana una contrasenya i comprova si està bé
    -   seguirItinerari -> Mostrar i anar resolent de manera ordenada tots els problemes
    -   mostrarProblemes -> Mostrar una llista amb tots els problemes
    -   escollirProblema -> Demana una ID i resol aquest problema en concret
    -   mostrarProgres -> Mostra l'historial dels problemes
    -   afegirProblema -> Demana les dades i insereix un problema nou
    -   reportarFeina -> Consulta el progrés i assigna una nota a l'alumne


## Conclusions i millores<a id="conclusions"></a>
Aquest projecte ha sigut tediós per mi perqué en un principi no estava familiaritzat amb l'entorn de base de dades amb Kotlin, però una vegada ja entrat en matèria i acostumat a fer les querys i connexions no ha estat gens malament.

Millores o errors:

- Al mostrar el progrés, estaría molt bé poder també, a part del número d'intents, mostrar les entrades que ha introduït l'usuari.
- S'hauria de controlar que no hi hagués repetició de noms d'usuari o iniciar sessió mitjançant el email, ja que si hagués diferents usuaris que s'anomenen igual hi hauria un problema.


###### Asier Barranco